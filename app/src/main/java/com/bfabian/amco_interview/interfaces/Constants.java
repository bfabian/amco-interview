package com.bfabian.amco_interview.interfaces;

/**
 * Created by bfabian on 07/11/18.
 */

public interface Constants {
    int REGULAR = 0,
        BOLD = 1,
        CONDENSED = 2,
        BOLD_CONDENSED = 3,
        SEMIBOLD = 4,
        LIGHT = 5;


    String CUSTOM_REGULAR = "fonts/MYRIADPRO-REGULAR.OTF";
    String CUSTOM_BOLD = "fonts/MYRIADPRO-BOLD.OTF";
    String CUSTOM_CONDENSED = "fonts/MYRIADPRO-COND.OTF";
    String CUSTOM_BOLD_CONDENSED = "fonts/MYRIADPRO-BOLDCOND.OTF";
    String CUSTOM_LIGHT = "fonts/MyriadPro-Light.otf";
    String CUSTOM_SEMIBOLD = "fonts/MYRIADPRO-SEMIBOLD.OTF";

    String SHOWLIST_KEY = "showList";
    String SHOWDETAIL_KEY = "showDetail";
    String IMAGE_TRANSITION_NAME = "imageTransitionName";

    String RECYCLERVIEW_POSITION = "position";

}
