package com.bfabian.amco_interview.interfaces;

import com.bfabian.amco_interview.models.CastItem;
import com.bfabian.amco_interview.models.ItemModel;
import com.bfabian.amco_interview.models.QueryModel;
import com.bfabian.amco_interview.models.Show;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitClient {
    @GET("schedule")
    Call<ArrayList<ItemModel>> getShowsPrincipalList(@Query("country") String country, @Query("date") String date);

    @GET("search/shows")
    Call<ArrayList<QueryModel>> searchShows(@Query("q") String query);

    @GET("shows/{show_id}")
    Call<Show> getShowInformation(@Path(value = "show_id") String showId);

    @GET("shows/{show_id}/cast")
    Call<ArrayList<CastItem>> getCastInformation(@Path(value = "show_id") String showId);
}
