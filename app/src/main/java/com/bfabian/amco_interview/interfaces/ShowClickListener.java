package com.bfabian.amco_interview.interfaces;

import android.widget.ImageView;

public interface ShowClickListener {
    void OnClickShow(long id, ImageView imageViewShowImage, String transitionName);
}
