package com.bfabian.amco_interview.views.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bfabian.amco_interview.CoreApp;
import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.adapters.CastAdapter;
import com.bfabian.amco_interview.interfaces.Constants;
import com.bfabian.amco_interview.models.CastItem;
import com.bfabian.amco_interview.models.Show;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomButton;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bfabian.amco_interview.utils.Utils.getCastComparator;
import static com.bfabian.amco_interview.utils.Utils.simplifyListToString;

public class FragmentShowDetail extends FragmentBase {
    public static final String TAG = FragmentShowDetail.class.getSimpleName();
    private Show mPrincipalData = null;
    private String imageTransitionName;
    private RecyclerView recyclerViewCast;
    private CastAdapter castAdapter;
    private LinearLayoutManager mLayoutManager;
    private int currentPosition = -1;

    private CustomButton buttonWebsite;
    private CustomTextView textViewSummary, textViewGenres, textViewSchedule, textViewShowName,
            textViewNetworkName, textViewRating;
    private ImageView imageViewShowImage;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_show_detail, container, false);
        textViewSummary = rootView.findViewById(R.id.txtv_summary);
        textViewGenres = rootView.findViewById(R.id.txtv_genres);
        textViewSchedule = rootView.findViewById(R.id.txtv_schedule);
        textViewShowName = rootView.findViewById(R.id.txtv_show_name);
        textViewNetworkName = rootView.findViewById(R.id.txtv_network_name);
        textViewRating = rootView.findViewById(R.id.txtv_rating);
        imageViewShowImage = rootView.findViewById(R.id.imgv_show_image);
        imageViewShowImage.setTransitionName(imageTransitionName);
        buttonWebsite = rootView.findViewById(R.id.btn_website);
        recyclerViewCast = rootView.findViewById(R.id.rcv_cast);
        setInfo();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            currentPosition = savedInstanceState.getInt(Constants.RECYCLERVIEW_POSITION);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.RECYCLERVIEW_POSITION,((LinearLayoutManager)recyclerViewCast.getLayoutManager()).findFirstVisibleItemPosition());
    }

    /**
     * Set info in correspondent views
     */
    private void setInfo(){
        if(mPrincipalData != null){
            setValue(textViewShowName, mPrincipalData.getName());
            setValue(textViewSummary, mPrincipalData.getSummary());
            setValue(textViewGenres, mResources.getString(R.string.tag_genre).concat(simplifyListToString(mPrincipalData.getGenres())));
            if(mPrincipalData.getSchedule() != null){
                setValue(textViewSchedule, mResources.getString(R.string.tag_schedule).concat(mPrincipalData.getSchedule().getTime()).concat(" | ").concat(simplifyListToString(mPrincipalData.getSchedule().getDays())));
            }
            if(mPrincipalData.getNetwork() != null){
                setValue(textViewNetworkName, mPrincipalData.getNetwork().getName());
            } else {
                setValue(textViewNetworkName, "");
            }
            if(mPrincipalData.getRating() != null && mPrincipalData.getRating().getAverage() != null){
                setValue(textViewRating, mPrincipalData.getRating().getAverage().toString());
            } else {
                setValue(textViewRating, "");
            }
            if(mPrincipalData.getImage() != null && mPrincipalData.getImage().getMedium() != null){
                setShowImage();
            }
            if(mPrincipalData.getOfficialSite() != null && !mPrincipalData.getOfficialSite().isEmpty()){
                buttonWebsite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = mPrincipalData.getOfficialSite();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    }
                });
            }
            getCast(String.valueOf(mPrincipalData.getId()));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
        buttonWebsite = null;
        textViewSummary = textViewGenres = textViewSchedule = textViewShowName = textViewNetworkName = textViewRating = null;
        imageViewShowImage = null;
    }

    /**
     * Get complete cast of the Show
     * @param Id Identifier of the person in the cast
     */
    private void getCast(String Id){
        Log.d(TAG, mPrincipalData.getName() + ": " + Id);
        Call<ArrayList<CastItem>> call;
        call = CoreApp.getInstance().getRetrofitClient().getCastInformation(Id);
        call.enqueue(new Callback<ArrayList<CastItem>>() {
            @Override
            public void onResponse(Call<ArrayList<CastItem>> call, Response<ArrayList<CastItem>> response) {
                switch (response.code()) {
                    case 200:
                        ArrayList<CastItem> data = response.body();
                        if(data != null){
                            castAdapter = new CastAdapter(data,mContext,getCastComparator());
                            mLayoutManager = new LinearLayoutManager(mContext,  LinearLayoutManager.HORIZONTAL, false);
                            recyclerViewCast.setLayoutManager(mLayoutManager);
                            recyclerViewCast.setAdapter(castAdapter);
                            if(currentPosition > 0){
                                recyclerViewCast.smoothScrollToPosition(currentPosition);
                            }
                        }
                        break;
                    case 401:

                        break;
                    default:
                        Log.d(TAG, "Error:" + response.code());
                        Log.d(TAG, response.raw().request().toString());
                        break;
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CastItem>> call, Throwable t) {

            }
        });

    }

    /**
     * set the correct image in correspondent target
     */
    private void setShowImage() {
        final RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(false)
                .placeholder(R.drawable.ic_image_gallery)
                .fitCenter();
        Glide.with(mContext)
                .load(mPrincipalData.getImage().getMedium())
                .apply(requestOptions)
                .into(imageViewShowImage);
    }


    private void setValue(CustomTextView viewTarget, String value){
        if(value != null && !value.isEmpty()){
            viewTarget.setText(Html.fromHtml(value));
        } else {
            viewTarget.setText(mResources.getString(R.string.not_aviable));
        }
    }

    /**
     * Getting instance of the Fragment
     * @param principalData Show Detail info
     * @param imageTransitionName name of the image element
     * @return new Instance of the Fragment
     */
    public static FragmentShowDetail newInstance (Show principalData, String imageTransitionName){
        FragmentShowDetail fragmentShowDetail = new FragmentShowDetail();
        fragmentShowDetail.setmPrincipalData(principalData);
        fragmentShowDetail.setImageTransitionName(imageTransitionName);
        return fragmentShowDetail;
    }

    public void setmPrincipalData(Show mPrincipalData) {
        this.mPrincipalData = mPrincipalData;
    }

    public void setImageTransitionName(String imageTransitionName) {
        this.imageTransitionName = imageTransitionName;
    }
}
