package com.bfabian.amco_interview.views.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.bfabian.amco_interview.CoreApp;
import com.bfabian.amco_interview.views.activities.BaseActivity;

public class FragmentBase extends Fragment {
    protected Context mContext;
    protected BaseActivity mActivity;
    protected Resources mResources;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = requireContext();
        mActivity = (BaseActivity) requireActivity();
        mResources = mContext.getResources();
    }
}
