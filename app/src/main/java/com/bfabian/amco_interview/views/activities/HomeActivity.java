package com.bfabian.amco_interview.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;


import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.interfaces.Constants;
import com.bfabian.amco_interview.models.ItemModel;
import com.bfabian.amco_interview.views.fragments.FragmentBase;
import com.bfabian.amco_interview.views.fragments.FragmentHome;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.bfabian.amco_interview.utils.DateHelper.getFormattedDate;

public class HomeActivity extends BaseActivity {

    private Toolbar mToolbar;
    private CustomTextView textViewToolbarTitle;
    private ArrayList<ItemModel> principalData = new ArrayList<>();

    private final static String TAG = HomeActivity.class.getSimpleName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolbar = findViewById(R.id.main_toolbar);
        textViewToolbarTitle = mToolbar.findViewById(R.id.txtv_toolbar_title);

        setSupportActionBar(this.mToolbar);


        textViewToolbarTitle.setText(getFormattedDate());


        if(getIntent() != null){
            principalData = (ArrayList<ItemModel>) getIntent().getSerializableExtra(Constants.SHOWLIST_KEY);
        }
        FragmentBase fragmentHome = FragmentHome.newInstance(principalData);
        setFragment(true, fragmentHome,FragmentHome.TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mToolbar = null;
        textViewToolbarTitle = null;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }


}
