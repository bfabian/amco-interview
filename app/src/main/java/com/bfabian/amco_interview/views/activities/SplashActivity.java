package com.bfabian.amco_interview.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.bfabian.amco_interview.CoreApp;
import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.interfaces.Constants;
import com.bfabian.amco_interview.models.ItemModel;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bfabian.amco_interview.utils.DateHelper.getISO8601FormattedDate;

public class SplashActivity extends BaseActivity {

    private final String TAG = SplashActivity.class.getSimpleName();
    Map<String, Serializable> mParams = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_empty);


        Call<ArrayList<ItemModel>> call = CoreApp.getInstance().getRetrofitClient().getShowsPrincipalList("US", getISO8601FormattedDate());
        call.enqueue(new Callback<ArrayList<ItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemModel>> call, Response<ArrayList<ItemModel>> response) {
                Log.d(TAG, String.format("Code: %d", response.code()));
                switch (response.code()) {
                    case 200:
                        ArrayList<ItemModel> data = response.body();
                        for (ItemModel itemModel: data){
                            Log.d(TAG, itemModel.getShow().getName() + " : ");
                            Log.d(TAG, "Official Site: " + itemModel.getShow().getOfficialSite());
                        }
                        mParams.put(Constants.SHOWLIST_KEY, data);
                        start(HomeActivity.class, mParams,true, null);
                        break;
                    case 401:

                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemModel>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
