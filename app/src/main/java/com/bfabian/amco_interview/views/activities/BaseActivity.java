package com.bfabian.amco_interview.views.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import com.bfabian.amco_interview.CoreApp;
import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.views.fragments.FragmentBase;

import java.io.Serializable;
import java.util.Map;

public class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private boolean tabletSize;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
    }

    public void start(Class<?> cls, Boolean startApp) {
        Intent intent = new Intent(this, cls);
        if (startApp) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        this.startActivity(intent);
    }

    public void start(Class<?> cls, Map<String, Serializable> params, Boolean startApp, ActivityOptionsCompat options) {
        Intent intent = new Intent(this, cls);
        if (startApp) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        for (Map.Entry<String,Serializable> entry: params.entrySet()){
            intent.putExtra(entry.getKey(), entry.getValue());
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if(options != null)
            this.startActivity(intent, options.toBundle());
        else
            this.startActivity(intent);
    }

    public void setFragment(boolean initFragment, FragmentBase fragment, String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!initFragment) {
            fragmentTransaction.addToBackStack(TAG);
        }
        fragmentTransaction.replace(R.id.container_main_home, fragment, TAG);
        fragmentTransaction.commit();
    }

    public boolean isTabletSize() {
        return tabletSize;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
