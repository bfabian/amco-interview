package com.bfabian.amco_interview.views.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.interfaces.Constants;
import com.bfabian.amco_interview.models.ItemModel;
import com.bfabian.amco_interview.models.Show;
import com.bfabian.amco_interview.views.fragments.FragmentBase;
import com.bfabian.amco_interview.views.fragments.FragmentHome;
import com.bfabian.amco_interview.views.fragments.FragmentShowDetail;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ShowDetailActivity extends BaseActivity {

    private Show principalData = null;
    private String imageTransitionName;


    private final static String TAG = ShowDetailActivity.class.getSimpleName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
        FragmentBase fragmentShowDetail;
        if (savedInstanceState != null) {
            fragmentShowDetail = (FragmentBase) getSupportFragmentManager().getFragment(savedInstanceState, "myFragmentName");
            principalData = (Show) savedInstanceState.getSerializable(Constants.SHOWDETAIL_KEY);
            imageTransitionName = savedInstanceState.getString(Constants.IMAGE_TRANSITION_NAME);
            ((FragmentShowDetail)fragmentShowDetail).setmPrincipalData(principalData);
            ((FragmentShowDetail)fragmentShowDetail).setImageTransitionName(imageTransitionName);
        } else {
            if(getIntent() != null){
                principalData = (Show) getIntent().getSerializableExtra(Constants.SHOWDETAIL_KEY);
                imageTransitionName = getIntent().getStringExtra(Constants.IMAGE_TRANSITION_NAME);
            }
            fragmentShowDetail  = FragmentShowDetail.newInstance(principalData, imageTransitionName);
        }
        setFragment(true, fragmentShowDetail,FragmentShowDetail.TAG);
        if(!isTabletSize()){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "myFragmentName", getActiveFragment());
        outState.putSerializable(Constants.SHOWDETAIL_KEY, principalData);
        outState.putString(Constants.IMAGE_TRANSITION_NAME, imageTransitionName);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     *
     * @return the current fragment
     */
    public FragmentBase getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return (FragmentBase) getSupportFragmentManager().findFragmentByTag(FragmentShowDetail.TAG);
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return (FragmentBase) getSupportFragmentManager().findFragmentByTag(tag);
    }
}
