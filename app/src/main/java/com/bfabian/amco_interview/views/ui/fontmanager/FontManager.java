package com.bfabian.amco_interview.views.ui.fontmanager;

import android.content.Context;
import android.graphics.Typeface;

import com.bfabian.amco_interview.interfaces.Constants;


import java.util.HashMap;
import java.util.Map;

import static com.bfabian.amco_interview.interfaces.Constants.BOLD;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_BOLD;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_BOLD_CONDENSED;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_CONDENSED;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_LIGHT;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_REGULAR;
import static com.bfabian.amco_interview.interfaces.Constants.CUSTOM_SEMIBOLD;


/**
 * Created by bfabian on 07/11/18.
 */

public class FontManager {
    private Map<String, Typeface> fontCache = new HashMap<String, Typeface>();
    private static FontManager instance = null;
    private Context mContext;

    private FontManager(Context mContext2) {
        mContext = mContext2;
    }

    public synchronized static FontManager getInstance(Context mContext) {

        if (instance == null) {
            instance = new FontManager(mContext);
        }
        return instance;
    }

    public Typeface loadFont(String font) {

        if (false == fontCache.containsKey(font)) {
            fontCache.put(font,
                    Typeface.createFromAsset(mContext.getAssets(), font));
        }
        return fontCache.get(font);
    }

    public static String selectTypeFace(String typeFace){
        if(typeFace == null){
            return CUSTOM_REGULAR;
        }
        switch (Integer.parseInt(typeFace)) {
            case Constants.REGULAR:
                return CUSTOM_REGULAR;
            case BOLD:
                return CUSTOM_BOLD;
            case Constants.CONDENSED:
                return CUSTOM_CONDENSED;
            case Constants.BOLD_CONDENSED:
                return CUSTOM_BOLD_CONDENSED;
            case Constants.LIGHT:
                return CUSTOM_LIGHT;
            case Constants.SEMIBOLD:
                return CUSTOM_SEMIBOLD;
            default:
                return CUSTOM_REGULAR;
        }
    }
}
