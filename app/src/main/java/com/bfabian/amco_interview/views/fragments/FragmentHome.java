package com.bfabian.amco_interview.views.fragments;

import android.animation.LayoutTransition;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bfabian.amco_interview.CoreApp;
import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.adapters.ShowAdapter;
import com.bfabian.amco_interview.interfaces.Constants;
import com.bfabian.amco_interview.interfaces.ShowClickListener;
import com.bfabian.amco_interview.models.ItemModel;
import com.bfabian.amco_interview.models.QueryModel;
import com.bfabian.amco_interview.models.Show;
import com.bfabian.amco_interview.utils.Utils;
import com.bfabian.amco_interview.views.activities.ShowDetailActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHome extends FragmentBase implements SearchView.OnQueryTextListener, ShowClickListener {
    public static final String TAG = FragmentHome.class.getSimpleName();
    private ArrayList<ItemModel> mPrincipalData = new ArrayList<>();
    private final ArrayList<ItemModel> mQueryData = new ArrayList<>();
    private RecyclerView recyclerViewShows;
    private ShowAdapter showAdapter;
    private LinearLayoutManager mLayoutManager;

    private SwipeRefreshLayout swipeRefreshLayout;

    private SearchView searchView;
    private MenuItem item;

    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerViewShows = null;
        swipeRefreshLayout = null;
        mLayoutManager = null;
        rootView = null;
        searchView = null;
        item = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerViewShows = rootView.findViewById(R.id.rcv_shows);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        showAdapter = new ShowAdapter(mPrincipalData, mContext, Utils.getShowComparatorDefault(), this);

        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerViewShows.setLayoutManager(mLayoutManager);
        recyclerViewShows.setAdapter(showAdapter);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_home, menu);
        item = mActivity.getToolbar().getMenu().findItem(R.id.menu_search);
        if(item != null){
            searchView = (SearchView) item.getActionView();
            searchView.setQueryHint("Buscar");
            searchView.setOnQueryTextListener(this);
            LinearLayout searchBar = searchView.findViewById(R.id.search_bar);
            searchBar.setLayoutTransition(new LayoutTransition());

            item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    swipeRefreshLayout.setEnabled(false);
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    swipeRefreshLayout.setEnabled(true);
                    resetList();
                    return true;
                }
            });

        }    }

    public static FragmentHome newInstance (ArrayList<ItemModel> principalData){
        FragmentHome fragmentHome = new FragmentHome();
        fragmentHome.setmPrincipalData(principalData);
        return fragmentHome;
    }

    private void setmPrincipalData(ArrayList<ItemModel> mPrincipalData) {
        this.mPrincipalData = mPrincipalData;
    }

    private void resetList(){
        showAdapter.setQuery(false);
        showAdapter.setmShowList(mPrincipalData);
        showAdapter.replaceAll(mPrincipalData);
        //showAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String query) {
        swipeRefreshLayout.setRefreshing(true);
        Call<ArrayList<QueryModel>> call;
        if(query == null || query.isEmpty()){
            resetList();
            swipeRefreshLayout.setRefreshing(false);
            return false;
        }
        call = CoreApp.getInstance().getRetrofitClient().searchShows(query);
        call.enqueue(new Callback<ArrayList<QueryModel>>() {
            @Override
            public void onResponse(Call<ArrayList<QueryModel>> call, Response<ArrayList<QueryModel>> response) {
                switch (response.code()) {
                    case 200:
                        ArrayList<QueryModel> data = response.body();
                        if(data != null){
                            mQueryData.clear();
                            for(QueryModel queryModel: data){
                                ItemModel newItemModel = new ItemModel();
                                newItemModel.setShow(queryModel.getShow());
                                mQueryData.add(newItemModel);
                            }
                            showAdapter.setQuery(true);
                            showAdapter.setmShowList(mQueryData);
                            showAdapter.replaceAll(mQueryData);
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        break;
                    case 401:

                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<QueryModel>> call, Throwable t) {

            }
        });
        return true;
    }

    /**
     * Launch Detail Activity for the selected show
     * @param id identifier of the show
     * @param imageViewShowImage imageView of the element in the recyclerView
     * @param transitionName name of the image element
     */
    @Override
    public void OnClickShow(long id, final ImageView imageViewShowImage, final String transitionName) {
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                mActivity,
                imageViewShowImage,
                transitionName);
        Call<Show> call;
        call = CoreApp.getInstance().getRetrofitClient().getShowInformation(String.valueOf(id));
        call.enqueue(new Callback<Show>() {
            @Override
            public void onResponse(Call<Show> call, Response<Show> response) {
                switch (response.code()) {
                    case 200:
                        Show data = response.body();
                        if(data != null){
                            Map<String, Serializable> mParams = new HashMap<>();
                            mParams.put(Constants.SHOWDETAIL_KEY, data);
                            mParams.put(Constants.IMAGE_TRANSITION_NAME, transitionName);
                            mActivity.start(ShowDetailActivity.class, mParams,false, options);
                        }
                        break;
                    case 401:

                        break;
                    default:
                        Log.d(TAG, "Error:" + response.code());
                        Log.d(TAG, response.raw().request().toString());
                        break;
                }
            }

            @Override
            public void onFailure(Call<Show> call, Throwable t) {

            }
        });
    }
}
