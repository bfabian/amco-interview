package com.bfabian.amco_interview.views.ui.fontmanager;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.bfabian.amco_interview.R;

public class CustomButton extends android.support.v7.widget.AppCompatButton {
    private String type_face;

    private static final String INSTANCE_STATE = "saved_instance_button";
    private static final String INSTANCE_TYPE_FACE = "type_face_button";

    public CustomButton(Context context) {
        this(context, null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.button);
    }
    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        try {

            TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.custom_font, defStyleAttr, 0);
            type_face = attributes.getString(R.styleable.custom_font_type);

            attributes.recycle();

            type_face = FontManager.selectTypeFace(type_face);

            setTypeface(FontManager.getInstance(getContext()).loadFont(type_face));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
