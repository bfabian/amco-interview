package com.bfabian.amco_interview;

import android.app.Application;

import com.bfabian.amco_interview.interfaces.RetrofitClient;
import com.bumptech.glide.request.target.ViewTarget;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoreApp extends Application {
    private static CoreApp instance;

    private RetrofitClient retrofitClient;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        ViewTarget.setTagId(R.id.glide_tag);
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.tvmaze.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        retrofitClient = retrofit.create(RetrofitClient.class);
    }

    /**
     * Getting instance of CoreApp
     *
     * @return CoreApp instance.
     */
    public static synchronized CoreApp getInstance() {
        return instance;
    }

    public RetrofitClient getRetrofitClient() {
        return retrofitClient;
    }
}
