package com.bfabian.amco_interview.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {

    /**
     *
     * @return date Capitalized
     */
    public static String getFormattedDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("EEEE dd 'de' MMMM yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        if(formattedDate != null && !formattedDate.isEmpty()){
            if(formattedDate.charAt(0) == ' '){
                formattedDate = formattedDate.substring(1);
            }
            String s1 = formattedDate.substring(0, 1).toUpperCase();
            formattedDate = s1 + formattedDate.substring(1);
        }
        return formattedDate;
    }

    /**
     *
     * @return date in format ISO_8601 (yyyy-mm-dd)
     */
    public static String getISO8601FormattedDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df.format(c);
    }

}
