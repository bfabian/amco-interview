package com.bfabian.amco_interview.utils;

import com.bfabian.amco_interview.models.CastItem;
import com.bfabian.amco_interview.models.ItemModel;

import java.util.Comparator;
import java.util.List;

public class Utils {
    /**
     * Used for sort the initial List of shows in alphabetical order
     * @param ascendent define the order in which it will be ordered (true -> A - Z || false -> Z - A)
     * @return Comparator sorted Alphabetical
     */
    public static Comparator<ItemModel> getShowComparatorAlphabetical(final boolean ascendent){
        return new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel itemModel, ItemModel itemModel2) {
                if(ascendent){
                    return itemModel.getName().compareTo(itemModel2.getName());
                }else {
                    return itemModel2.getName().compareTo(itemModel.getName());
                }
            }
        };
    }

    /**
     * Used for sort the initial List of shows in original order
     * @return Comparator organized in Original order
     */
    public static Comparator<ItemModel> getShowComparatorDefault(){
        return new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel itemModel, ItemModel itemModel2) {
                return (int) (itemModel.getLocalId() - itemModel2.getLocalId());
            }
        };
    }

    /**
     * Used for sort the cast List in original order
     * @return Comparator organized in Original order
     */
    public static Comparator<CastItem> getCastComparator(){
        return new Comparator<CastItem>() {
            @Override
            public int compare(CastItem itemModel, CastItem itemModel2) {
                return (int) (itemModel.getLocalId() - itemModel2.getLocalId());
            }
        };
    }

    /**
     * Used for decide where you get the value to display
     * @param show POJO where data info is obtained
     * @param isQuery define type of value (true -> return schedule time| false -> air date)
     * @return correct info
     */
    public static String getAirDate(ItemModel show, boolean isQuery){
        return isQuery ? show.getShow().getSchedule().getTime(): show.getAirdate();
    }

    /**
     * Used for decide where you get the value to display
     * @param show POJO where data info is obtained
     * @param isQuery define type of value (true -> return schedule days| false -> air time)
     * @return correct info
     */
    public static String getAirTime(ItemModel show, boolean isQuery){
        return isQuery ? simplifyListToString(show.getShow().getSchedule().getDays()): show.getAirtime();
    }

    /**
     *
     * @param stringList original list
     * @return Single String with all values of the Array with coma.
     */
    public static String simplifyListToString(List<String> stringList){
        String output = "";
        for (int index = 0; index < stringList.size(); index++){
            if(index < stringList.size() - 1)
                output = output.concat(stringList.get(index)).concat(", ");
            else
                output = output.concat(stringList.get(index));
        }
        return output;
    }

}
