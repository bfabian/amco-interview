package com.bfabian.amco_interview.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class CastItem implements Serializable {
    private static AtomicLong nextId = new AtomicLong();

    private Long localId;


    @SerializedName("person")
    @Expose
    private Person person;

    @SerializedName("character")
    @Expose
    private Character character;

    @SerializedName("self")
    @Expose
    private boolean self;

    @SerializedName("voice")
    @Expose
    private boolean voice;

    public CastItem() {
        this.localId = nextId.getAndIncrement();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    public boolean isVoice() {
        return voice;
    }

    public void setVoice(boolean voice) {
        this.voice = voice;
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }
}
