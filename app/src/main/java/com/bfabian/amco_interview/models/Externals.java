
package com.bfabian.amco_interview.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Externals implements Serializable {

    @SerializedName("imdb")
    @Expose
    private String imdb;
    @SerializedName("thetvdb")
    @Expose
    private Long thetvdb;
    @SerializedName("tvrage")
    @Expose
    private Long tvrage;

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public Long getThetvdb() {
        return thetvdb;
    }

    public void setThetvdb(Long thetvdb) {
        this.thetvdb = thetvdb;
    }

    public Long getTvrage() {
        return tvrage;
    }

    public void setTvrage(Long tvrage) {
        this.tvrage = tvrage;
    }

}
