
package com.bfabian.amco_interview.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Schedule implements Serializable {


    @SerializedName("days")
    @Expose
    private List<String> days;

    @SerializedName("time")
    @Expose
    private String time;

    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
