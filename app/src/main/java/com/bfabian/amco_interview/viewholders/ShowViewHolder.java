package com.bfabian.amco_interview.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;

public class ShowViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageViewShowImage;
    public CustomTextView textViewShowName, textViewNetworkName, textViewAirDate, textViewAirTime;
    public View viewSeparator;

    public ShowViewHolder(@NonNull View itemView) {
        super(itemView);

        imageViewShowImage = itemView.findViewById(R.id.imgv_show_image);
        textViewShowName = itemView.findViewById(R.id.txtv_show_name);
        textViewNetworkName = itemView.findViewById(R.id.txtv_network_name);
        textViewAirDate = itemView.findViewById(R.id.txtv_air_date);
        textViewAirTime = itemView.findViewById(R.id.txtv_air_time);
        viewSeparator = itemView.findViewById(R.id.view_separator);
    }
}
