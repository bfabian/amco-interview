package com.bfabian.amco_interview.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;

public class CastViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageViewCast;
    public CustomTextView textViewCast;


    public CastViewHolder(@NonNull View itemView) {
        super(itemView);

        imageViewCast = itemView.findViewById(R.id.imgv_cast_image);
        textViewCast = itemView.findViewById(R.id.txtv_person_name);

    }
}
