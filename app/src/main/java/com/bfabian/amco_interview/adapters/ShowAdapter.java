package com.bfabian.amco_interview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.interfaces.ShowClickListener;
import com.bfabian.amco_interview.models.ItemModel;
import com.bfabian.amco_interview.viewholders.ShowViewHolder;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static com.bfabian.amco_interview.utils.Utils.getAirDate;
import static com.bfabian.amco_interview.utils.Utils.getAirTime;

public class ShowAdapter extends RecyclerView.Adapter<ShowViewHolder> {
    private final static String TAG = ShowAdapter.class.getSimpleName();
    private ArrayList<ItemModel> mShowList = new ArrayList<>();
    private SortedList<ItemModel> mShowSortedList;
    private Context mContext;
    private Comparator<ItemModel> mComparator;
    private int lastPosition = -1;
    ShowClickListener mShowClickListener;

    private boolean isQuery = false;

    /**
     *
     * @param mShowList Original List
     * @param comparator Compatator for order list
     * @param showClickListener listener for actions when a show is selected
     */
    public ShowAdapter(ArrayList<ItemModel> mShowList, Context context, Comparator<ItemModel> comparator, ShowClickListener showClickListener) {
        setmShowList(mShowList);
        this.mContext = context;
        this.mComparator = comparator;
        mShowSortedList = new SortedList<ItemModel>(ItemModel.class, new SortedList.Callback<ItemModel>() {
            @Override
            public int compare(ItemModel itemModel, ItemModel itemModel2) {
                return mComparator.compare(itemModel, itemModel2);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(ItemModel oldItem, ItemModel newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(ItemModel item1, ItemModel item2) {
                return item1.getId() == item2.getId();
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
        replaceAll(mShowList);
        this.mShowClickListener = showClickListener;
    }

    /**
     * Set complete list to SortedList
     * @param models new List
     */
    public void replaceAll(List<ItemModel> models) {
        mShowSortedList.beginBatchedUpdates();
        for (int i = mShowSortedList.size() - 1; i >= 0; i--) {
            final ItemModel model = mShowSortedList.get(i);
            if (!models.contains(model)) {
                mShowSortedList.remove(model);
            }
        }
        mShowSortedList.addAll(models);
        mShowSortedList.endBatchedUpdates();
    }

    @NonNull
    @Override
    public ShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_item, parent, false);
        return new ShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShowViewHolder showViewHolder, int position) {
        try {
            final ItemModel currentShow = mShowSortedList.get(position);
            setAnimation(showViewHolder.itemView, position);
            showViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mShowClickListener != null){
                        Log.d(TAG, currentShow.toString());
                        mShowClickListener.OnClickShow(currentShow.getShow().getId(), showViewHolder.imageViewShowImage, "showImage");
                    }
                }
            });
            if(position == 0){
                showViewHolder.viewSeparator.setVisibility(View.GONE);
            } else {
                showViewHolder.viewSeparator.setVisibility(View.VISIBLE);
            }
            if(currentShow != null){
                setValue(showViewHolder.textViewAirDate, getAirDate(currentShow, isQuery));
                setValue(showViewHolder.textViewAirTime, getAirTime(currentShow, isQuery));
                if(currentShow.getShow() != null){
                    setValue(showViewHolder.textViewShowName, currentShow.getShow().getName());
                    if(currentShow.getShow().getNetwork() != null){
                        setValue(showViewHolder.textViewNetworkName, currentShow.getShow().getNetwork().getName());
                    }
                    if(currentShow.getShow().getImage() != null){
                        showViewHolder.imageViewShowImage.setTag(mShowSortedList.get(position));
                        setShowImage(mShowSortedList.get(position), showViewHolder);
                    }
                }
            }
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            Log.i(TAG, e.toString());
            e.printStackTrace();
        }
    }

    /**
     *
     * @param viewTarget textview target
     * @param value String for display
     */
    private void setValue(CustomTextView viewTarget, String value){
        if(value != null && !value.isEmpty()){
            viewTarget.setText(value);
        } else {
            viewTarget.setText(mContext.getResources().getString(R.string.not_aviable));
        }
    }

    /**
     * set the correct image in correspondent target
     * @param itemModel POJO where data info is obtained
     * @param showViewHolder viewHolder target
     */
    private void setShowImage(ItemModel itemModel, final ShowViewHolder showViewHolder) {
        Log.d(TAG, itemModel.getShow().getImage().getMedium());
        final RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(false)
                .placeholder(R.drawable.ic_image_gallery)
                .fitCenter();
        final ItemModel serieTag = (ItemModel) showViewHolder.imageViewShowImage.getTag();
        if (Objects.equals(serieTag, itemModel)) {
            Glide.with(mContext)
                    .load(itemModel.getShow().getImage().getMedium())
                    .apply(requestOptions)
                    .into(showViewHolder.imageViewShowImage);
        }
    }

    /**
     * Set new list
     * @param mShowList is the new List
     */
    public void setmShowList(ArrayList<ItemModel> mShowList) {
        if(this.mShowList != null)
            this.mShowList.clear();
        else
            this.mShowList = new ArrayList<>();
        this.mShowList.addAll(mShowList);
    }

    @Override
    public int getItemCount() {
        return mShowSortedList.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    /**
     * Set criterion for decide if show correct info
     * @param query (true => is list for a query and display Schedule days | false => is initial list and display AirDate)
     */
    public void setQuery(boolean query) {
        isQuery = query;
    }
}
