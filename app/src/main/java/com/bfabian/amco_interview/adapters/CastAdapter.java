package com.bfabian.amco_interview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bfabian.amco_interview.R;
import com.bfabian.amco_interview.models.CastItem;
import com.bfabian.amco_interview.viewholders.CastViewHolder;
import com.bfabian.amco_interview.views.ui.fontmanager.CustomTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public class CastAdapter extends RecyclerView.Adapter<CastViewHolder> {
    private final static String TAG = CastAdapter.class.getSimpleName();
    private ArrayList<CastItem> mCastList = new ArrayList<>();
    private SortedList<CastItem> mCastSortedList;
    private Context mContext;
    private Comparator<CastItem> mComparator;
    private int lastPosition = -1;

    /**
     *
     * @param mCastList Original List
     * @param context
     * @param comparator Compatator for order list
     */
    public CastAdapter(ArrayList<CastItem> mCastList, Context context, Comparator<CastItem> comparator) {
        setmCastList(mCastList);
        this.mContext = context;
        this.mComparator = comparator;
        mCastSortedList = new SortedList<CastItem>(CastItem.class, new SortedList.Callback<CastItem>() {
            @Override
            public int compare(CastItem itemModel, CastItem itemModel2) {
                return mComparator.compare(itemModel, itemModel2);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(CastItem oldItem, CastItem newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(CastItem item1, CastItem item2) {
                return item1.getPerson().getId() == item2.getPerson().getId();
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
        replaceAll(mCastList);
    }


    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cast_item, parent, false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CastViewHolder castViewHolder, int position) {
        try {
            final CastItem currentCast = mCastSortedList.get(position);
            setAnimation(castViewHolder.itemView, position);
            if(currentCast.getPerson() != null){
                setValue(castViewHolder.textViewCast, currentCast.getPerson().getName());
            }
            castViewHolder.imageViewCast.setTag(currentCast);
            //if(currentCast.getPerson() != null && currentCast.getPerson().getImage() != null)
            setCastImage(currentCast, castViewHolder);
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            Log.i(TAG, e.toString());
            e.printStackTrace();
        }
    }

    /**
     *
     * @param viewTarget textview target
     * @param value String for display
     */
    private void setValue(CustomTextView viewTarget, String value){
        if(value != null && !value.isEmpty()){
            viewTarget.setText(value);
        } else {
            viewTarget.setText(mContext.getResources().getString(R.string.not_aviable));
        }
    }

    /**
     * set the correct image in correspondent target
     * @param castItem POJO where data info is obtained
     * @param castViewHolder viewHolder target
     */
    private void setCastImage(CastItem castItem, final CastViewHolder castViewHolder) {
        final RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(false)
                .placeholder(R.drawable.ic_image_gallery)
                .fitCenter();
        final CastItem serieTag = (CastItem) castViewHolder.imageViewCast.getTag();
        if (Objects.equals(serieTag, castItem)) {
            if(castItem.getPerson().getImage() != null){
                Glide.with(mContext)
                        .load(castItem.getPerson().getImage().getMedium())
                        .apply(requestOptions)
                        .into(castViewHolder.imageViewCast);
            } else {
                castViewHolder.imageViewCast.setImageDrawable(mContext.getDrawable(R.drawable.ic_image_gallery));
            }
        }
    }

    /**
     * Set new list
     * @param mCastList
     */
    private void setmCastList(ArrayList<CastItem> mCastList) {
        if(this.mCastList != null)
            this.mCastList.clear();
        else
            this.mCastList = new ArrayList<>();
        this.mCastList.addAll(mCastList);
    }

    /**
     * Set complete list to SortedList
     * @param models new List
     */
    private void replaceAll(List<CastItem> models) {
        mCastSortedList.beginBatchedUpdates();
        for (int i = mCastSortedList.size() - 1; i >= 0; i--) {
            final CastItem model = mCastSortedList.get(i);
            if (!models.contains(model)) {
                mCastSortedList.remove(model);
            }
        }
        mCastSortedList.addAll(models);
        mCastSortedList.endBatchedUpdates();
    }


    @Override
    public int getItemCount() {
        return mCastSortedList.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
